//
//  DatePickerViewController.swift
//  Symo New
//
//  Created by Apple on 06/04/21.
//

import UIKit

protocol SelectDate {
    func selectedDate(timestamp:Date)
}

class DatePickerViewController: UIViewController {
    
    static var shared = DatePickerViewController()
    var dateDelegate: SelectDate? = nil
    
    //MARK: IBOutlets
    @IBOutlet weak var datePicker: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
    }
    
    
    //MARK: IBActions
    @IBAction func doneButton(_ sender: Any) {
        self.dateDelegate?.selectedDate(timestamp: datePicker.date)
        self.dismiss(animated: true, completion: nil)
    }
    
}
