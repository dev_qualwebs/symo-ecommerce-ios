//
//  AppDelegate.swift
//  Symo New
//
//  Created by Apple on 30/03/21.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces
import Firebase
import UserNotifications
import Google

var bundle = Bundle(path: Bundle.main.path(forResource: (Singleton.shared.language == "ar" ? Singleton.shared.language : "Base"), ofType: "lproj")!)

@available(iOS 13.0, *)
@main
class AppDelegate: UIResponder, UIApplicationDelegate {

   var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window?.backgroundColor = .white
        
        GMSServices.provideAPIKey(GOOGLE_API_KEY)
        GMSPlacesClient.provideAPIKey(GOOGLE_API_KEY)
        return true
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        NotificationCenter.default.post(name: NSNotification.Name(N_HANDLE_LOCATION_PERMISSION), object: nil)
    }

    func changeStatusBarColor(){
        if #available(iOS 13.0, *) {
           let app = UIApplication.shared
           let statusBarHeight: CGFloat = app.statusBarFrame.size.height

           let statusbarView = UIView()
           statusbarView.backgroundColor = UIColor.red
            window?.addSubview(statusbarView)
           statusbarView.translatesAutoresizingMaskIntoConstraints = false
           statusbarView.heightAnchor
             .constraint(equalToConstant: statusBarHeight).isActive = true
           statusbarView.widthAnchor
            .constraint(equalTo: window!.widthAnchor, multiplier: 1.0).isActive = true
           statusbarView.topAnchor
            .constraint(equalTo: window!.topAnchor).isActive = true
           statusbarView.centerXAnchor
            .constraint(equalTo: window!.centerXAnchor).isActive = true

        } else {
              let statusBar = UIApplication.shared.value(forKeyPath:
           "statusBarWindow.statusBar") as? UIView
              statusBar?.backgroundColor = UIColor.red
        }
    }
}

