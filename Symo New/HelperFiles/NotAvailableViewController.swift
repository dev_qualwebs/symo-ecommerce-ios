//
//  NotAvailableViewController.swift
// Hey hala
//
//  Created by qw on 19/01/21.
//

import UIKit

class NotAvailableViewController: UIViewController {
    
    //MARK: IBOutlets
    
    @IBOutlet weak var titleText: UILabel!
    
    var heading = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleText.text = heading
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3)) {
          self.dismiss(animated: false, completion: nil)
        }
        
    }
    //MARK: IBActions
    @IBAction func dismissAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}
