//
//  SplashScreen4ViewController.swift
//  Symo New
//
//  Created by Apple on 30/03/21.
//

import UIKit

class SplashScreen4ViewController: UIViewController {

    //MARK: IBOutlet
    @IBOutlet weak var buttongetStarted: UIButton!
    @IBOutlet weak var imageEng: UIImageView!
    @IBOutlet weak var imageArb: UIImageView!
    
    var langugage: String = "en"
    var arabicToggle: String = "false"
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        buttongetStarted.backgroundColor = K_PINK_COLOR
      //  buttongetStarted.setTitleColor(.white, for: .normal)
    }

    //MARK: Action
    @IBAction func selectEnglish(_ sender: UIButton) {
        imageArb.image = #imageLiteral(resourceName: "Ellipse 3 copy")
        imageEng.image = #imageLiteral(resourceName: "Ellipse 3 copy 2")
        langugage = "en"
        arabicToggle = "false"
    }
    
    @IBAction func selectArabic(_ sender: UIButton) {
        imageEng.image = #imageLiteral(resourceName: "Ellipse 3 copy")
        imageArb.image = #imageLiteral(resourceName: "Ellipse 3 copy 2")
        langugage = "ar"
        arabicToggle = "true"
    }
    
    @IBAction func getStarted(_ sender: UIButton) {
       // SessionManager.sharedInstance.saveAppLanguage(withLanguage: langugage)
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: arabicToggle)
        UserDefaults.standard.set(encodedData, forKey: "arabicToggle")
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(myVC, animated: true)
       // NavigationController.shared.goToHomeVC(controller: self)
    }
}
