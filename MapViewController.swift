//
//  MapViewController.swift
//  Symo New
//
//  Created by Apple on 19/04/21.
//

import UIKit
import GooglePlaces
import GoogleMapsUtils
import GoogleMaps
import MapKit


class MapViewController: UIViewController, SelectFromPicker {
    func selectedPickerData(val: String, pos: Int) {
        self.selectedCategory = self.categoryData[pos]
        self.categoryName.text = val
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var categoryName: UITextField!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var addressField: UITextField!
    
    
    var categoryData = [CategoryResponse]()
    var selectedCategory = CategoryResponse()
    
    //My Location
    var location: CLLocation!
    lazy var geocoder = CLGeocoder()
    var marker = GMSMarker()
    private var clusterManager: GMUClusterManager!
    
    //Vendors
    var vendorLocation = [Address]()
    
    var lat : Double?
    var long : Double?
    
    var isNearBy: Bool = true

    var categoryId = 0
    var selectedMarkerIndex = 0

    

    override func viewDidLoad() {
        super.viewDidLoad()
        NavigationController.shared.getAllCategory { (response) in
            self.categoryData = response
        }
        
        
        self.mapView.settings.myLocationButton = true
        
        /*Get Location For User*/
        NavigationController.shared.registerEventListenerForLocation { (location) in
            Helper().initMap(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 10, googleMapView: self.mapView)
            
            self.location = location
            if self.isNearBy {
                //API call vendor pins
                self.apiCallForVendorsPin()
            } else {
                self.markPinOnMap(coordinate: location.coordinate)
                self.getAddressFromCoordinates(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            }
        }
        
        if isNearBy {
          setUpCluster()
        }
       
        //This will get the Users Current Location
        NavigationController.shared.getUserCurrentLocation()
        mapView.delegate = self
    }
    
    func apiCallForVendorsPin(){
        let latitude = optionalLat + "\(location.coordinate.latitude)"
        let longitude =   "&long=" + "\(location.coordinate.longitude)"
        var completeUrl = ""
        
        if categoryId == 0 {
            completeUrl = "/location" + "\(latitude)" + "\(longitude)" + (lang + (Singleton.shared.language ?? ""))
        } else {
            completeUrl = "/location" + "\(categoryId)" + "\(latitude)" + "\(longitude)" + (lang + (Singleton.shared.language ?? ""))
        }
        
        SessionManager.shared.methodForApiCalling(url: completeUrl, method: .get, parameter: nil, objectClass: MapResponse.self, requestCode: completeUrl, userToken: nil) { (address) in
            
            self.vendorLocation = address.response
            self.clusterManager.clearItems()
            
           // DispatchQueue.main.async {
                if address.response.count > 0 {
                    // Generate and add random items to the cluster manager.
                    self.generateClusterItems()
                }
                else{
                    self.mapView.clear()
                }
           // }
        }
    }
    
    func setUpCluster() {
        // Set up the cluster manager with the supplied icon generator and
        // renderer.
        let iconGenerator = GMUDefaultClusterIconGenerator()
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: mapView,
                                                 clusterIconGenerator: iconGenerator)
        clusterManager = GMUClusterManager(map: mapView, algorithm: algorithm,
                                           renderer: renderer)
        
        // Call cluster() after items have been added to perform the clustering
        // and rendering on map.
        clusterManager.cluster()
    }
    
    /// Randomly generates cluster items within some extent of the camera and
    /// adds them to the cluster manager.
    private func generateClusterItems() {
        mapView.clear()
        clusterManager.clearItems()
        let extent = 0.001
        for vendorLocation in vendorLocation {
            let lat = Double(vendorLocation.lat ?? 0) + extent * randomScale()
            let lng = Double(vendorLocation.long ?? 0) + extent * randomScale()
            let name = vendorLocation.vendor_name
            let item = POIItem(position: CLLocationCoordinate2DMake(lat, lng), name: name ?? "No Name")
            clusterManager.add(item)
        }
        clusterManager.cluster()
    }
    
    /// Returns a random value between -1.0 and 1.0.
    private func randomScale() -> Double {
        return Double(arc4random()) / Double(UINT32_MAX) * 2.0 - 1.0
    }
    
    //MARK: IBActions
    @IBAction func categoryAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        myVC.pickerDelegate = self
        for val in self.categoryData{
            myVC.pickerData.append(val.category_name ?? "")
        }
        myVC.headingLabel = "Select Category"
        if(self.categoryData.count > 0){
         self.navigationController?.present(myVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addressAction(_ sender: Any) {
        let acController = GMSAutocompleteViewController()
           acController.delegate = self
        self.present(acController, animated: true, completion: nil)
    }
    
    
    @IBAction func clearAction(_ sender: Any) {
        self.addressField.text = ""
    }
}

extension MapViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        if !isNearBy {
            getAddressFromCoordinates(latitude: coordinate.latitude, longitude: coordinate.longitude)
            markPinOnMap(coordinate: coordinate)
        }
    }
    
    func markPinOnMap(coordinate: CLLocationCoordinate2D) {
        self.location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        self.lat = coordinate.latitude
        self.long = coordinate.longitude
        self.marker.position.longitude =  coordinate.longitude
        self.marker.position.latitude =  coordinate.latitude
        self.marker.map = self.mapView
        self.marker.icon = GMSMarker.markerImage(with: .blue)
        self.mapView?.camera = GMSCameraPosition.camera(withTarget: marker.position, zoom: 10.0)
    }
    
    
    func getAddressFromCoordinates(latitude: CLLocationDegrees, longitude: CLLocationDegrees)
    {
        let location = CLLocation(latitude: latitude, longitude: longitude)
        geocoder.reverseGeocodeLocation(location) { (placemarks, error) in
            var placeMark: CLPlacemark!
            placeMark = placemarks?[0]
            let addressDict = String(describing: (placemarks?[0].addressDictionary as Any))
            if placeMark != nil {
                if addressDict != "nil" {
                    let address = placeMark.addressDictionary?["FormattedAddressLines"] as? AnyObject
                    let array_address = address as! NSArray
                    let city = String(describing: (placeMark.addressDictionary!["City"] as AnyObject))
                    let subAdministrativeArea = String(describing: (placeMark.addressDictionary!["SubAdministrativeArea"] as AnyObject))
                    let state = String(describing: (placeMark.addressDictionary!["State"] as AnyObject))
                    for var index in 0..<array_address.count
                    {
                        if index == 0 {
                            self.addressField.text = String(describing: array_address[index])
                        } else {
                            self.addressField.text = self.addressField.text! + ", " + String(describing: array_address[index])
                        }
                    }
                }
                else {
                    self.addressField.text = ""
                }
            } else {
                self.addressField.text = ""
            }
        }
    }
    
}


extension MapViewController: GMSAutocompleteViewControllerDelegate {

  // Handle the user's selection.
  func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
    print("Place name: \(place.name)")
    print("Place address: \(place.formattedAddress)")
    print("Place attributions: \(place.attributions)")
    self.addressField.text = place.formattedAddress
    dismiss(animated: true, completion: nil)
  }

  func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
    // TODO: handle the error.
    print("Error: \(error)")
    dismiss(animated: true, completion: nil)
  }

  // User cancelled the operation.
  func wasCancelled(_ viewController: GMSAutocompleteViewController) {
    print("Autocomplete was cancelled.")
    dismiss(animated: true, completion: nil)
  }
}


class POIItem: NSObject, GMUClusterItem {
    var position: CLLocationCoordinate2D
    var name: String!
    
    init(position: CLLocationCoordinate2D, name: String) {
        self.position = position
        self.name = name
    }
}
