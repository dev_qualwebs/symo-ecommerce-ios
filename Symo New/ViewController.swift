//
//  ViewController.swift
//  Symo New
//
//  Created by Apple on 30/03/21.
//

import UIKit
import CoreLocation


class ViewController: UIViewController, CLLocationManagerDelegate, Confirmation {
    func confirmationSelection(action: Int) {
        if(action == 1){
            UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
        }else {
          self.handleUserLocation()
        }
       
    }
    
    
    lazy var locationManager = CLLocationManager()
    var timer = Timer()

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.showErrorMessage(_:)), name: NSNotification.Name(N_SHOW_ERROR_MESSAGE), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleUserLocation), name: NSNotification.Name(N_HANDLE_LOCATION_PERMISSION), object: nil)
        NavigationController.shared.getCountries { (val) in
            NavigationController.shared.getCities(countryId: val[0].id ?? 0) { (val) in
                
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.handleUserLocation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    @objc func handleTimer(){
        self.handleUserLocation()
    }
    
   @objc func handleUserLocation(){
        switch CLLocationManager.authorizationStatus() {
        case .authorizedAlways,.authorizedWhenInUse:
            locationManager.delegate = self
            if let loc = locationManager.location {
                Singleton.shared.userLocation = loc
                self.setInitialController()
                self.timer.invalidate()
            }
        case .notDetermined:
            locationManager.delegate = self
            locationManager.requestWhenInUseAuthorization()
            self.timer.invalidate()
            self.timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(handleTimer), userInfo: nil, repeats: true)
        case .denied:
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmationPopupViewController") as! ConfirmationPopupViewController
            myVC.confirmationDelegate = self
            myVC.secondTxt = true
            myVC.confirmationText = "Allow Location"
            myVC.detailText = "Symo needs to access your location for get nearby shops and stores."
            myVC.modalPresentationStyle = .overFullScreen
            self.navigationController?.present(myVC, animated: true, completion: nil)
        case .restricted:
            // Nothing you can do, app cannot use location services
            break
        }
    }
    
    @objc func showErrorMessage(_ notif: NSNotification){
        if let val = notif.userInfo?["msg"] as? String{
            self.showErrorMsg(msg:val)
        }
    }
    
    func setInitialController(){
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }


}

