//
//  SubCategoryViewController.swift
//  Symo New
//
//  Created by Apple on 17/04/21.
//

import UIKit

class SubCategoryViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var pinkView: UIView!
    @IBOutlet weak var noDataLabel: DesignableUILabel!
    @IBOutlet weak var subCategoryCollection: UICollectionView!
    
    var categoryId = Int()
    var subCategoryData = [CategoryResponse]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = K_PINK_COLOR
        self.pinkView.backgroundColor = K_PINK_COLOR
        
        self.getSubcategoryData()
    }
    
    func getSubcategoryData(){
        ActivityIndicator.show(view: self.view)
        let url = U_BASE + "user/\(self.categoryId)/sub_category" + pageLimitWithLang + Singleton.shared.selectedLanguage
        SessionManager.shared.methodForApiCalling(url: url, method: .get, parameter: nil, objectClass: GetCategory.self, requestCode: url, userToken: nil) { (response) in
            ActivityIndicator.hide()
            self.subCategoryData = response.response
            self.subCategoryCollection.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension SubCategoryViewController: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(self.subCategoryData.count == 0){
            self.noDataLabel.isHidden = false
        }else {
            self.noDataLabel.isHidden = false
        }
        return self.subCategoryData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionCell", for: indexPath) as! CategoryCollectionCell
        let val = self.subCategoryData[indexPath.row]
        if((val.category_avatar ?? "").contains("http")){
            cell.shopImage.sd_setImage(with: URL(string: (val.category_avatar ?? "")), placeholderImage: #imageLiteral(resourceName: "nearbyshop"))
        }else {
            cell.shopImage.sd_setImage(with: URL(string:U_IMAGE_BASE + (val.category_avatar ?? "")), placeholderImage: #imageLiteral(resourceName: "nearbyshop"))
        }
        cell.shopName.text = val.sub_category_name
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.size.width/2)-15, height:150)
    }
}
