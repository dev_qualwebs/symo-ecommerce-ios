//
//  SortbyViewController.swift
//  Symo New
//
//  Created by Apple on 07/04/21.
//

import UIKit


class SortbyViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var sortbyTable: UITableView!
    
    
    let sortByData = ["Most Popular", "Relevance", "New Arrivals", "Price low to high", "Price high to low"]
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: IBAction
    @IBAction func dismissAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    

}

extension SortbyViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sortByData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableCell") as! MenuTableCell
        if(indexPath.row == K_SELECTED_SORTBY){
            cell.menuImage.image = #imageLiteral(resourceName: "Ellipse 3 copy")
        }else {
            cell.menuImage.image = #imageLiteral(resourceName: "Ellipse 3 copy 2")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        K_SELECTED_SORTBY = indexPath.row
        self.sortbyTable.reloadData()
    }
}
