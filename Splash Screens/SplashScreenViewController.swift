//
//  SplashScreenViewController.swift
//  Symo New
//
//  Created by Apple on 30/03/21.
//

protocol CustomUserActivityDelegate {
    func handleUserActivity()
}

import UIKit

class SplashScreenViewController: UIViewController {
    
    //MARK: IBOutlet
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var userActivityDelegate: CustomUserActivityDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        PageViewController.index_delegate = self
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
}
extension SplashScreenViewController: ControllerIndexDelegate {
    func getControllerIndex(index: Int) {
        pageControl.currentPage = index
        pageControl.isHidden = false
        if index == 0 {
        } else if index == 3 {
            pageControl.isHidden = true
        } else {
        }
    }
}
