//
//  FilterScreenViewController.swift
//  Symo New
//
//  Created by Apple on 07/04/21.
//

import UIKit

var FILTER_SUBDOMAIN = ""

class FilterScreenViewController: UIViewController {

    var filterData = [FilterForShopResponse]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getFilterData()
    }
    
    func getFilterData(){
        ActivityIndicator.hide()
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_FILTER_SHOP + FILTER_SUBDOMAIN, method: .get, parameter: nil, objectClass: GetFilterForShop.self, requestCode: U_GET_FILTER_SHOP, userToken: nil) { (response) in
            
        }
    }
}

extension FilterScreenViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.filterData.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.filterData[section].name_en ?? ""
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filterData[section].attribute_option.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableCell") as! MenuTableCell
        return cell
    }

}
