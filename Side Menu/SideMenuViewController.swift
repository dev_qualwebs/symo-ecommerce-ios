//
//  SideMenuViewController.swift
//  Symo New
//
//  Created by Apple on 03/04/21.
//

import UIKit

class SideMenuViewController: UIViewController {
    //MARK: IBOutlets
    
    @IBOutlet weak var loginView: View!
    
    let loggedIn = ["Reward Points", "My Orders", "Shipping Address",  "Payments", "My Favourites", "Notification", "Offers", "Share", "Settings", "", "", "Contact Us", "FAQ's", "Privacy Policy", "Disclaimer", "Terms & Condition" ]
    let nonLoggedIn = ["My Orders", "Shipping Address", "Payments", "My Favourites", "Notifications", "Manage Business", "Offers", "Share", "Setting"]

    override func viewDidLoad() {
        super.viewDidLoad()
        if(Singleton.shared.userDetail.id != nil){
            self.loginView.isHidden = false
        }else {
            self.loginView.isHidden = true
        }
        self.view.backgroundColor = K_PINK_COLOR
    }
    
}

extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(Singleton.shared.userDetail.id != nil){
            return loggedIn.count
        }else {
            return nonLoggedIn.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableCell", for: indexPath) as! MenuTableCell
        if(Singleton.shared.userDetail.id != nil){
        cell.menuLabel.text  = self.loggedIn[indexPath.row]
        }else {
            cell.menuLabel.text  = self.nonLoggedIn[indexPath.row]
        }
        return cell
   }
}

class MenuTableCell: UITableViewCell {
    //MARK: IBoutlets
    @IBOutlet weak var menuLabel: DesignableUILabel!
    @IBOutlet weak var menuImage: UIImageView!
    
}
