//
//  PageViewController.swift
//  Symo New
//
//  Created by Apple on 30/03/21.
//

import UIKit

protocol ControllerIndexDelegate {
    func getControllerIndex(index: Int)
}

class PageViewController: UIPageViewController {
    
    var orderedViewControllers: [UIViewController] = []

    //Define Static variable
    static var dataSource1: UIPageViewControllerDataSource?
    static var index_delegate: ControllerIndexDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataSource = self
        handleView()
        setControllers()
        
        //Calling custom datasource for managing controller of Page view controller from other pages on a tap of (NEXT) button.
        PageViewController.dataSource1 = self
    }
    
    //Initializing Controller
    func handleView() {
        orderedViewControllers = [self.newViewController(controller: "SplashScreen1ViewController"),
                                  self.newViewController(controller: "SplashScreen2ViewController"),
                                  self.newViewController(controller: "SplashScreen3ViewController"),
                                  self.newViewController(controller: "SplashScreen4ViewController")]
    }
    
    func setControllers() {
        if let lastViewController = orderedViewControllers.last {
            setViewControllers([lastViewController], direction: .forward, animated: true, completion: nil)
        }
        setViewControllers([orderedViewControllers[2]], direction: .forward, animated: true, completion: nil)
        setViewControllers([orderedViewControllers[1]], direction: .forward, animated: true, completion: nil)
        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
        }
    }
    
    //Assigning controller manually
    func nextController(index: Int, direction: UIPageViewController.NavigationDirection) {
        setViewControllers([orderedViewControllers[index]], direction: direction, animated: true, completion: nil)
        PageViewController.index_delegate?.getControllerIndex(index: index)
    }
    
    //Getting current controller index
    func currentControllerIndex(VC: UIViewController) {
        if let viewControllerIndex = orderedViewControllers.index(of: VC) {
            PageViewController.index_delegate?.getControllerIndex(index: viewControllerIndex)
        }
    }
    
    private func newViewController(controller: String) -> UIViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: controller)
    }
}

//MARK: Page view controller delegate methods
extension PageViewController: UIPageViewControllerDataSource
{
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        currentControllerIndex(VC: viewController)
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        let previousIndex = viewControllerIndex - 1
        guard previousIndex >= 0 else {
            return nil
        }
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        currentControllerIndex(VC: viewController)
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        let nextIndex = viewControllerIndex + 1
        guard orderedViewControllers.count != nextIndex else {
            return nil
        }
        guard orderedViewControllers.count > nextIndex else {
            return nil
        }
        return orderedViewControllers[nextIndex]
    }
}
