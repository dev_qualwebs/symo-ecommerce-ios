//
//  Singleton.swift
//  FFK-KIOSK
//
//  Created by qw on 18/01/21.
//

import UIKit
import LocalAuthentication
import CoreLocation


class Singleton {
    
    static let shared = Singleton()
    var userLocation = CLLocation()
    var userDetail = getUserDetail()
    var vendorsByCity = [VendorResponse]()
    var countryData = [Countries]()
    var cityData = [Cities]()
    var selectedCity = getCity()
    var selectedCountry = getCountry()
    var newTrendingBusiness = NewTrendingResponse()
    var allCategoryData = [CategoryResponse]()
    var selectedLanguage = "en"
    var seasonalOffer = SeasonalOffersResponse()
  
    var language:String?{
        get {
            if let appLanguage = UserDefaults.standard.value(forKey: K_APP_LANGUAGE) as? String {
                return appLanguage
            }
         //   Singleton.shared.selectedLanguage = appLa
            return "en"
        } set {
            saveAppLanguage(withLanguage: newValue!)
        }
    }
    
    public static func getCity() -> Cities {
        do{
            if let cityData = UserDefaults.standard.value(forKey: UD_SELECTED_CITY) {
                if let city = try! JSONDecoder().decode(Countries.self, from: cityData as! Data) as? Cities{
                   
                    return city
                }
            }else {
                return Cities()
            }
            
        }catch {
            print("error saving details")
        }
        return Cities()
    }
    
    public static func getCountry() -> Countries {
        do{
            if let countryData = UserDefaults.standard.value(forKey: UD_SELECTED_COUNTRY) {
                if let country = try! JSONDecoder().decode(Countries.self, from: countryData as! Data) as? Countries{
                 
                    return country
                }
            }else {
                return Countries()
            }
            
        }catch {
            print("error saving details")
        }
        return Countries()
    }
    
    func saveUserDetail(user: UserDetail) {
        do{
            let encode =  try? JSONEncoder().encode(user)
            UserDefaults.standard.setValue(encode, forKey: UD_USER_DETAIl)
        }catch {
            print("error saving details")
        }
    }
    
    public static func getUserDetail() -> UserDetail {
        if let user = UserDefaults.standard.value(forKey: UD_USER_DETAIl) {
            if let data = try! JSONDecoder().decode(UserDetail.self, from: user as! Data) as? UserDetail{
                return data
            }
        }else {
            return UserDetail()
        }
        return UserDetail()
    }
    
    
    // userDetail = SearchUserResponse()
    //Save application lanaguage
    func saveAppLanguage(withLanguage appLang: String)->Void{
        UserDefaults.standard.set(appLang, forKey: K_APP_LANGUAGE)
        UserDefaults.standard.synchronize()
        changeApplicationLanguage(language: appLang)
    }
    
    //Change Application language : This WIll be used when toggle on off
    func changeApplicationLanguage(language: String) -> Void{
        UserDefaults.standard.set(language, forKey: K_APP_LANGUAGE)
        UserDefaults.standard.synchronize()
        setUIViewAccToLanguage(language: language)
    }
    
    func setUIViewAccToLanguage(language: String) {
        if language == "ar" {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        } else {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }
        bundle = Bundle(path: Bundle.main.path(forResource: (language == "ar" ? language : "Base"), ofType: "lproj")!)
    }
    
    //GET Current Language
    func getCurrentLanguage() -> String {
        var lang = ""
        if let data = UserDefaults.standard.string(forKey: K_APP_LANGUAGE) as? String {
            lang = language!
            setUIViewAccToLanguage(language: lang)
        }
        else{
            print("There is an issue")
        }
        return lang
    }
}

