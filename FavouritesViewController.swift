//
//  FavouritesViewController.swift
//  Symo New
//
//  Created by Apple on 01/04/21.
//

import UIKit

class FavouritesViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var pinkView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = K_PINK_COLOR
        self.pinkView.backgroundColor = K_PINK_COLOR
    }
    

    //MARK: IBActions
    @IBAction func menuAction(_ sender: Any) {
        self.menu()
    }

}
