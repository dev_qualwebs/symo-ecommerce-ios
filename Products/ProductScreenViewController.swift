//
//  ProductScreenViewController.swift
//  Symo New
//
//  Created by Apple on 17/04/21.
//

import UIKit

class ProductScreenViewController: UIViewController {
    //MARK: IBOutlets
    
    @IBOutlet weak var noProductLabel: DesignableUILabel!
    var productData = ShopProductResponse()
    var subdomain = String()
    var filterParam:[String:Any] = [
        "color": [],
        "price": [],
        "size": [],
        "rating": "any"
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = K_PINK_COLOR
        self.getShopProduct()
    }
    
    func getShopProduct(){
        ActivityIndicator.show(view: self.view)
        let param: [String:Any] = [
            "lang": "en",
            "category_id" : "",
            "sort_by": K_SELECTED_SORTBY + 1,
            "filter": self.filterParam
        ]
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_SHOP_PRODUCT + subdomain, method: .post, parameter: param, objectClass: GetVendorDetail.self, requestCode: U_GET_SHOP_PRODUCT, userToken: U_GET_SHOP_PRODUCT) { (response) in
            ActivityIndicator.hide()
        }
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func filterAction(_ sender: Any) {
        FILTER_SUBDOMAIN = self.subdomain
        self.openFilter()
    }
    
    @IBAction func sortAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SortbyViewController") as! SortbyViewController
        self.present(myVC, animated: true, completion: nil)
    }
    
}

extension ProductScreenViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryTableCell") as! CategoryTableCell
        return cell
    }
    
    
}
