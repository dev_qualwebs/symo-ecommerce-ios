//
//  SplashScreen1ViewController.swift
//  Symo New
//
//  Created by Apple on 30/03/21.
//

import UIKit

class SplashScreen1ViewController: UIViewController {

    //MARK: IBOutlet
    @IBOutlet weak var labelHeading: UILabel!
    @IBOutlet weak var labelDetails: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //MARK: Action
    @IBAction func skip(_ sender: UIButton) {
        
        UserDefaults.standard.synchronize()
      //  SessionManager.sharedInstance.saveAppLanguage(withLanguage: "en")
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: "false")
        UserDefaults.standard.set(encodedData, forKey: "arabicToggle")
        
        NavigationController.shared.goToHomeVC(controller: self)
    }

    @IBAction func next(_ sender: UIButton) {
        let pageVC = self.parent as! PageViewController
        pageVC.nextController(index: 1, direction: .forward)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
}
