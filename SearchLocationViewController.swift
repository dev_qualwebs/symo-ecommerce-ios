//
//  SearchLocationViewController.swift
//  Symo New
//
//  Created by Apple on 20/04/21.
//

import UIKit

class SearchLocationViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var locationTable: ContentSizedTableView!
    
    var countryData = [Countries]()
    var cityData = [Cities]()
    var currentData = 1

    override func viewDidLoad() {
        super.viewDidLoad()
        NavigationController.shared.getCountries { (val) in
            self.countryData = val
        }
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension SearchLocationViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(currentData == 1){
            return self.countryData.count
        }else {
            return self.cityData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableCell") as!  MenuTableCell
        cell.menuLabel.text = currentData == 1 ? self.countryData[indexPath.row].name_en: self.cityData[indexPath.row].name_en
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(currentData == 1){
            Singleton.shared.cityData = []
            ActivityIndicator.show(view: self.view)
            NavigationController.shared.getCities(countryId: self.countryData[indexPath.row].country_id ?? 0) { (val) in
                self.currentData = 2
                self.cityData = val
                self.locationTable.reloadData()
                ActivityIndicator.hide()
            }
        }else {
            Singleton.shared.selectedCity = self.cityData[indexPath.row]
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
}
