//
//  LoginViewController.swift
//  Symo New
//
//  Created by Apple on 30/03/21.
//

import UIKit
import SkyFloatingLabelTextField

class LoginViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var loginLabel: DesignableUILabel!
    @IBOutlet weak var emailFIeld: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var passwordField: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var forgotPassword: CustomButton!
    @IBOutlet weak var loginButton: CustomButton!
    @IBOutlet weak var orLabel: DesignableUILabel!
    @IBOutlet weak var noAccountLabel: DesignableUILabel!
    @IBOutlet weak var signupButton: CustomButton!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var pinkView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //TextFields
        self.view.backgroundColor = K_PINK_COLOR
        self.pinkView.backgroundColor = K_PINK_COLOR
        self.loginButton.backgroundColor = K_PINK_COLOR
        
        let helper = Helper()
        let textFields = [emailFIeld,  passwordField]
        for index in 0..<textFields.count {
            helper.setupFields(textField: textFields[index]! , index: index, form: K_LOGIN_FORM)
        }
        forgotPassword.setTitle(forgotPassword.titleLabel?.text?.localized, for: .normal)
    }
    
    //MARK: IBActions
    @IBAction func forgotPasswordAction(_ sender: Any) {
    }
    
    @IBAction func loginAction(_ sender: Any) {
        if(self.emailFIeld.text!.isEmpty){
            self.showErrorMsg(msg: "Enter Email Address")
        }else if !(self.isValidEmail(emailStr: self.emailFIeld.text ?? "")){
            self.showErrorMsg(msg: "Enter valid Email Address")
        }else if(self.passwordField.text!.isEmpty){
            self.showErrorMsg(msg: "Enter Password")
        }else {
            let param:[String:Any] = [
                "email":self.emailFIeld.text ?? "",
                "password":self.passwordField.text ?? "",
                "lang": Singleton.shared.selectedLanguage
            ]
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_LOGIN
                                                      , method: .post, parameter: param, objectClass: LoginResponse.self, requestCode: U_LOGIN, userToken: nil) { (response) in
                ActivityIndicator.hide()
                self.showErrorMsg(msg: "Logged In Successfully")
                Singleton.shared.saveUserDetail(user: response.response)
                Singleton.shared.userDetail = response.response
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
                self.navigationController?.pushViewController(myVC, animated: true)
            }
        }
    }
    
    @IBAction func signupAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SignupViewController") as! SignupViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

