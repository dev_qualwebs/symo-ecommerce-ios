//
//  ConfirmationPopupViewController.swift
//  Symo New
//
//  Created by Apple on 06/04/21.
//

import UIKit

protocol Confirmation{
    func confirmationSelection(action: Int)
}


class ConfirmationPopupViewController: UIViewController {
    
    //MARK: IBOutlets
    @IBOutlet weak var confirmationLabel: UILabel!
    @IBOutlet weak var secondText: UILabel!
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var okButton: UIButton!
    
    
    var confirmationDelegate: Confirmation? = nil
    var secondTxt = true
    
    var isConfirmationViewHidden = true
    var confirmationText = String()
    var detailText = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(secondTxt){
            self.secondText.isHidden = false
            self.okButton.setTitle("YES", for: .normal)
            self.cancelButton.setTitle("NO", for: .normal)
        }else {
            self.secondText.isHidden = true
        }
        self.confirmationLabel.text = self.confirmationText
        self.secondText.text = self.detailText
    }
    
    //MARK: IBAction
    
    @IBAction func okAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
        self.confirmationDelegate?.confirmationSelection(action: 1)
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
        self.confirmationDelegate?.confirmationSelection(action: 2)
       
    }
    
    @IBAction func confirmOkAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
        self.confirmationDelegate?.confirmationSelection(action: 1)
        
    }
    
}
