//
//  HomeViewController.swift
//  Symo New
//
//  Created by Apple on 01/04/21.
//

import UIKit
import ImageSlideshow
import SDWebImage
import Cosmos

class HomeViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var imageSlider: ImageSlideshow!
    @IBOutlet weak var newCollection: UICollectionView!
    
    @IBOutlet weak var trendingCollection: UICollectionView!
    @IBOutlet weak var categoryCollection: UICollectionView!
    @IBOutlet weak var nearShopTable: ContentSizedTableView!
    @IBOutlet weak var pinkView: UIView!
    @IBOutlet weak var selectedCountry: CustomButton!
    @IBOutlet weak var noOfferLabel: DesignableUILabel!

    var vendorByCityData = [VendorResponse]()
    var shopnewTrendingData = NewTrendingResponse()
    var allCategoryData = [CategoryResponse]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = K_PINK_COLOR
        self.pinkView.backgroundColor = K_PINK_COLOR
        self.nearShopTable.estimatedRowHeight = 120
        self.nearShopTable.rowHeight = UITableView.automaticDimension
        self.handleAPICalling()
       
        imageSlider.slideshowInterval = 3.0
        imageSlider.draggingEnabled = true
        
        imageSlider.pageIndicatorPosition = .init(horizontal: .center, vertical: .under)
        imageSlider.contentScaleMode = UIViewContentMode.scaleAspectFill
        let pageIndicator = UIPageControl()
        pageIndicator.currentPageIndicatorTintColor = UIColor.lightGray
        pageIndicator.pageIndicatorTintColor = UIColor.black
        imageSlider.pageIndicator = pageIndicator
        imageSlider.activityIndicator = DefaultActivityIndicator()
        imageSlider.delegate = self

        let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTap))
        imageSlider.addGestureRecognizer(recognizer)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.selectedCountry.setTitle(Singleton.shared.selectedCity.name_en ?? "", for: .normal)
    }

    @objc func didTap() {
        let fullScreenController = imageSlider.presentFullScreenController(from: self)
        // set the activity indicator for full screen controller (skipping the line will show no activity indicator)
        fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
    }
    
    func handleAPICalling(){
        NavigationController.shared.getAllSeasonalOffer { (response) in
            if((response.seasonal_offers?.count ?? 0) > 0){
                var input = [SDWebImageSource]()
                for val in response.seasonal_offers! {
                    input.append(SDWebImageSource(urlString:val.template ?? "")!)
                }
                self.imageSlider.setImageInputs(input)
                self.noOfferLabel.isHidden = true
            }else {
                self.noOfferLabel.isHidden = false
            }
        }
        
        
        NavigationController.shared.getvendorByCity { (response) in
            self.vendorByCityData = response
            self.nearShopTable.reloadData()
        }
        
        NavigationController.shared.getNewAndTrending { (response) in
            self.shopnewTrendingData = response
            self.newCollection.reloadData()
            self.trendingCollection.reloadData()
        }
        
        NavigationController.shared.getAllCategory { (response) in
            self.allCategoryData = response
            self.categoryCollection.reloadData()
        }
    }
    
    //MARK: IBActions
    @IBAction func menuAction(_ sender: Any) {
        self.menu()
    }
    
    @IBAction func nearbyAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
}

extension HomeViewController: ImageSlideshowDelegate {
    func imageSlideshow(_ imageSlideshow: ImageSlideshow, didChangeCurrentPageTo page: Int) {
        print("current page:", page)
    }
}


extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == newCollection){
            return self.shopnewTrendingData.new_business.count
        }else if(collectionView == trendingCollection){
            return self.shopnewTrendingData.trending_business.count
        }else  {
            return self.allCategoryData.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionCell", for: indexPath) as! CategoryCollectionCell
        if(collectionView == newCollection){
            let val =  self.shopnewTrendingData.new_business[indexPath.row]
            if((val.avatar ?? "").contains("http")){
                cell.shopImage.sd_setImage(with: URL(string: (val.avatar ?? "")), placeholderImage: #imageLiteral(resourceName: "nearbyshop"))
            }else {
                cell.shopImage.sd_setImage(with: URL(string:U_IMAGE_BASE + (val.avatar ?? "")), placeholderImage: #imageLiteral(resourceName: "nearbyshop"))
            }
            cell.shopName.text = val.vendor_name
            cell.shopDistance.isHidden = true
        }else if(collectionView == trendingCollection){
            let val = self.shopnewTrendingData.trending_business[indexPath.row]
            if((val.avatar ?? "").contains("http")){
                cell.shopImage.sd_setImage(with: URL(string: (val.avatar ?? "")), placeholderImage: #imageLiteral(resourceName: "nearbyshop"))
            }else {
                cell.shopImage.sd_setImage(with: URL(string:U_IMAGE_BASE + (val.avatar ?? "")), placeholderImage: #imageLiteral(resourceName: "nearbyshop"))
            }
            cell.shopName.text = val.vendor_name
            cell.shopDistance.isHidden = true
        }else {
            let val = self.allCategoryData[indexPath.row]
            if((val.category_avatar ?? "").contains("http")){
                cell.shopImage.sd_setImage(with: URL(string: (val.category_avatar ?? "")), placeholderImage: #imageLiteral(resourceName: "nearbyshop"))
            }else {
                cell.shopImage.sd_setImage(with: URL(string:U_IMAGE_BASE + (val.category_avatar ?? "")), placeholderImage: #imageLiteral(resourceName: "nearbyshop"))
            }
            cell.shopName.text = val.category_name
            cell.shopDistance.isHidden = true
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
        if(collectionView == newCollection){
            myVC.vendorId =  self.shopnewTrendingData.new_business[indexPath.row].vendor_id ?? 0
            self.navigationController?.pushViewController(myVC, animated: true)
        }else if(collectionView == trendingCollection){
            myVC.vendorId =  self.shopnewTrendingData.trending_business[indexPath.row].vendor_id ?? 0
            self.navigationController?.pushViewController(myVC, animated: true)
        }else {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "SubCategoryViewController") as! SubCategoryViewController
            controller.categoryId = self.allCategoryData[indexPath.row].category_id ?? 0
            self.navigationController?.pushViewController(controller, animated: true)
        }
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 120, height: 150)
    }
    
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.vendorByCityData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryTableCell") as! CategoryTableCell
        let val = self.vendorByCityData[indexPath.row]
        if((val.avatar ?? "").contains("http")){
            cell.shopImage.sd_setImage(with: URL(string: (val.avatar ?? "")), placeholderImage: #imageLiteral(resourceName: "nearbyshop"))
        }else {
            cell.shopImage.sd_setImage(with: URL(string:U_IMAGE_BASE + (val.avatar ?? "")), placeholderImage: #imageLiteral(resourceName: "nearbyshop"))
        }
        cell.shopName.text = val.vendor_name
        cell.shopDistance.text = "\(Int(val.distance ?? 0)) kms"
     
        //cell.shopAddress.text = 
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
        myVC.vendorId = self.vendorByCityData[indexPath.row].vendor_id ?? 0
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
}

class CategoryCollectionCell: UICollectionViewCell {
    //MARK: IBOutlets
    @IBOutlet weak var shopImage: ImageView!
    @IBOutlet weak var shopName: DesignableUILabel!
    @IBOutlet weak var shopDistance: DesignableUILabel!
}

class CategoryTableCell: UITableViewCell {
    //MARK: IBOutlets
    @IBOutlet weak var shopImage: ImageView!
    @IBOutlet weak var shopName: DesignableUILabel!
    @IBOutlet weak var shopDistance: DesignableUILabel!
    @IBOutlet weak var shopAddress: DesignableUILabel!
    @IBOutlet weak var shopTime: DesignableUILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var totalReview: DesignableUILabel!
    
}
