//
//  Extensions.swift
//  Hey hala
//
//  Created by qw on 23/12/20.

import UIKit
import CoreLocation
import SkyFloatingLabelTextField
import SideMenu

extension UIViewController {
    func showErrorMsg(msg: String){
        if let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as? NotAvailableViewController{
            myVC.heading = msg
            myVC.modalPresentationStyle = .overFullScreen
            self.present(myVC, animated: false, completion: nil)
        }
    }
    
    
    func addTransition(direction: CATransitionSubtype,controller: UIViewController) {
        let transition:CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.push
        transition.subtype = direction
        controller.navigationController?.view.layer.add(transition, forKey: kCATransition)
    }
    
    func calculateTimeDifference(date1: Int, date2: Int) -> Int{
        let d1 = Date(timeIntervalSince1970: TimeInterval(date1))
        let d2 = Date(timeIntervalSince1970: TimeInterval(date2))
        let diff = d2.timeIntervalSince(d1)
        return Int(diff)
    }
    
    
    func convertTimestampToDate(_ timestamp: Int, to format: String) -> String {
        var myVal = Int()
        var intValue:Int64 = 10000000000
        if(timestamp/Int(truncatingIfNeeded: intValue) == 0){
            myVal = timestamp
        }else {
            myVal = timestamp/1000
        }
        let date = Date(timeIntervalSince1970: TimeInterval(myVal))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: date)
    }
    
    func openUrl(urlStr: String) {
        let url = URL(string: urlStr)!
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    func changeColor(string:String,colorString: String,color: UIColor,field:UILabel){
        let main_string = string
        var string_to_color = colorString
        //colorString.font
        var range = (main_string as NSString).range(of: string_to_color)
        
        let attribute = NSMutableAttributedString.init(string: main_string)
        if let font = UIFont(name: "Poppins-SemiBold", size: 20) {
            let fontAttributes = [NSAttributedString.Key.font: font]
            let size = (string_to_color as NSString).size(withAttributes: fontAttributes)
        }
        attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: color , range: range)
        field.attributedText = attribute
    }
    
    func isValidEmail(emailStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }
    
    func hasLocationPermission() -> Bool {
        var hasPermission = false
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                hasPermission = false
            case .authorizedAlways, .authorizedWhenInUse:
                hasPermission = true
            }
        } else {
            hasPermission = false
        }
        
        return hasPermission
    }
    
    func callNumber(phoneNumber:String) {
        if let phoneCallURL = URL(string: "telprompt://\(phoneNumber)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                if #available(iOS 10.0, *) {
                    application.open(phoneCallURL, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                    application.openURL(phoneCallURL as URL)
                }
            }
        }
    }
    
    @objc func menu() {
        SideMenuManager.default.menuFadeStatusBar = false
        guard let sideMenuNavController =  SideMenuManager.defaultManager.menuLeftNavigationController else {
            let sideMenuController = storyboard?.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
            SideMenuManager.defaultManager.menuWidth = self.view.frame.width
            SideMenuManager.defaultManager.menuLeftNavigationController = UISideMenuNavigationController(rootViewController: sideMenuController)
            SideMenuManager.defaultManager.menuLeftNavigationController?.setNavigationBarHidden(true, animated: false)
            present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
            return
        }
        present(sideMenuNavController, animated: true, completion: nil)
    }
    
    @objc func openFilter() {
        SideMenuManager.default.menuFadeStatusBar = false
        guard let sideMenuNavController =  SideMenuManager.defaultManager.menuLeftNavigationController else {
            let sideMenuController = storyboard?.instantiateViewController(withIdentifier: "FilterScreenViewController") as! FilterScreenViewController
            SideMenuManager.defaultManager.menuWidth = self.view.frame.width
            
            SideMenuManager.defaultManager.menuRightNavigationController = UISideMenuNavigationController(rootViewController: sideMenuController)
            SideMenuManager.defaultManager.menuRightNavigationController?.setNavigationBarHidden(true, animated: false)
            
            present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
            return
        }
        present(sideMenuNavController, animated: true, completion: nil)
    }
    
    
}

extension String {
    func applyPatternOnNumbers(pattern: String, replacmentCharacter: Character) -> String {
        var pureNumber = self.replacingOccurrences( of: "[^0-9]", with: "", options: .regularExpression)
        for index in 0 ..< pattern.count {
            guard index < pureNumber.count else { return pureNumber }
            let stringIndex = String.Index(encodedOffset: index)
            let patternCharacter = pattern[stringIndex]
            guard patternCharacter != replacmentCharacter else { continue }
            pureNumber.insert(patternCharacter, at: stringIndex)
        }
        return pureNumber
    }
    
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
}

extension UIImageView {
    func changeTint(color: UIColor) {
        let templateImage =  self.image?.withRenderingMode(.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
}

final class ContentSizedTableView: UITableView {
    override var contentSize:CGSize {
        didSet {
            invalidateIntrinsicContentSize()
        }
    }
    override var intrinsicContentSize: CGSize {
        layoutIfNeeded()
        return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height)
    }
    
}

extension UIPageControl {
    
    func customPageControl(dotFillColor:UIColor, dotBorderColor:UIColor, dotBorderWidth:CGFloat) {
        for (pageIndex, dotView) in self.subviews.enumerated() {
            dotView.frame.size = CGSize(width: 15, height: 5)
            if self.currentPage == pageIndex {
                dotView.backgroundColor = dotFillColor
            }else{
                dotView.backgroundColor = .clear
            }
        }
    }
}
