//
//  SplashScreen2ViewController.swift
//  Symo New
//
//  Created by Apple on 30/03/21.
//
import UIKit

class SplashScreen2ViewController: UIViewController {

    //MARK: IBOutlet
    @IBOutlet weak var labelHeading: UILabel!
    @IBOutlet weak var labelDetails: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }

    //MARK: Action
    @IBAction func next(_ sender: UIButton) {
        let pageVC = self.parent as! PageViewController
        pageVC.nextController(index: 2, direction: .forward)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
