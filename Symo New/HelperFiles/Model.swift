//
//  Model.swift
//  Hey Hala
//
//  Created by qw on 09/01/21.
//

import Foundation

struct ErrorResponse: Codable{
    var message: String?
  //  var status: Int?
}

struct SuccessResponse: Codable{
    var message: String?
  //  var status: Int?
}

struct LoginResponse: Codable {
    var status: Bool?
    var response = UserDetail()
    var error: String?
}
    
struct UserDetail: Codable{
    var id: Int?
    var email: String?
    var name: String?
    var country_code: String?
    var phone: String?
    var avatar: String?
    var city: String?
    var otp: Int?
    var lat: Double?
    var long: Double?
    var is_verified: Int?
    var role_id: Int?
    var access_token: String?
    var gcm_token: String?
    var notification: String?
    var social_login: String?
    var social_id: String?
    var city_id: Int?
    var country_id: Int?
    var flag_deleted: Int?
    var logout: Int?
    var seen: Int?
    var email_verified: Int?
    var city_image: String?
    var user_type: Int?
    var manage_business: Int?
    var membership_id: String?
}

struct GetVendor: Codable {
    var status: Bool?
    var response = GetVendorResponse()
}

struct GetVendorResponse: Codable {
    var response = [VendorResponse]()
    var count: Int?
}

struct VendorResponse: Codable {
    var vendor_id: Int?
    var city_id: Int?
    var vendor_name: String?
    var reviews: Int?
    var is_premium: Int?
    var avatar: String?
    var offers: Int?
    var rating: Int?
    var distance: Double?
}

struct CountryResponse: Codable {
    var status: String?
    var countries = [Countries]()
}

struct Countries: Codable {
    var id: Int?
    var name_en: String?
    var name_ar: String?
    var country_code: String?
    var country_images: String?
    var latitude: Double?
    var longitude: Double?
    var country_id: Int?
    var name_ur: String?
    var name_es: String?
    var name_pt: String?
    var name_zh: String?
}

struct CityResponse: Codable {
    //var status:String?
    var cities = [Cities]()
    var country = Countries()
}

struct Cities: Codable {
    var name_en: String?
    var name_ar: String?
    var city_images: String?
    var country_id: Int?
    var city_id: Int?
}

struct GetNewTrending: Codable {
    var status: Bool?
    var response = NewTrendingResponse()
}

struct NewTrendingResponse: Codable {
    var trending_business = [BusinessDetail]()
    var new_business = [BusinessDetail]()
}

struct BusinessDetail: Codable{
    var id: Int?
    var vendor_id: Int?
    var user_id: Int?
    var name: String?
    var review:String?
    var is_approved: Int?
    var report: Int?
    var report_seen: Int?
    var seen: Int?
    var last_updated_by: Int?
    var vendor_name: String?
    var phone: String?
    var premium_plan_id: Int?
    var mark_popular: Int?
    var avatar: String?
    var address: String?
}

struct GetCategory: Codable {
    var status: Bool?
    var response = [CategoryResponse]()
}

struct CategoryResponse: Codable {
    var id: Int?
    var name_en: String?
    var name_ar: String?
    var category_image: String?
    var category_id: Int?
    var category_name: String?
    var category_avatar: String?
    var subcategory_count: Int?
    var sub_category_id: Int?
    var sub_category_name: String?
    var name_es: String?
    var name_pt: String?
    var name_zh: String?
    var name_ur: String?
}

struct GetSeasonalOffer: Codable {
    var status: Bool?
    var response = SeasonalOffersResponse()
    
}

struct SeasonalOffersResponse: Codable {
    var seasonal_offers: [SeasonalOffers]?
}

struct SeasonalOffers: Codable {
    let template: String?
    let vendor_id: Int?
}

struct GetShopProducts: Codable {
    var status: Bool?
    var response = ShopProductResponse()
}

struct ShopProductResponse: Codable {
    var business_products = [ProductResponse]()
    var total_count: Int?
    var categories = [CategoryResponse]()
}

struct ProductResponse: Codable {
    var id: Int?
    var name_en: String?
    var product_type: Int?
    var name_ar: String?
    var name_ur: String?
    var name_es: String?
    var name_pt: String?
    var name_zh: String?
    var sku: String?
    var slug: String?
    var price: String?
    var tax: String?
    var discount: String?
    var business_id: Int?
    var category_id: Int?
    var subcategory_id: Int?
    var keyboard: String?
    var default_image: String?
    var description: String?
    var is_customizable: Int?
    var is_returnable: Int?
    var is_exchangable: Int?
    var returnable_within_days: Int?
    var responsibility: Int?
    var status: Int?
    var stock_status: Int?
    var is_deleted: Int?
    var is_featured: Int?
    var qty: Int?
    var exchangable_within_days: Int?
    var parent_product_id:Int?
    var parent_product_slug: String?
    var variation_id: Int?
    var name: String?
    var product_category = CategoryResponse()
}

struct GetVendorDetail: Codable {
    var status: Bool?
    var response = VendorDetail()
}

struct VendorDetail: Codable {
    var lat: Double?
    var long: Double?
    var vendor_id: Int?
    var subdomain: String?
    var user_id: Int?
    var vendor_name: String?
    var meta_title: String?
    var phone: String?
    var phone2: String?
    var phone3: String?
    var whatsapp_number: String?
    var address: String?
    var rating: String?
    var avatar: String?
    var background_image: String?
    var description: String?
    var is_verified: Int?
    var city_id: Int?
    var city_name: String?
    var notify: Int?
    var bookmark: Int?
}

struct Address: Codable {
    let vendor_name: String?
    let long: Float?
    let lat: Float?
    let vendor_id: Int?
    let distance: Float?
}

//MARK: Map
struct MapResponse: Codable {
    let status: Bool
    let response = [Address]()
    let error: ErrorResponse?
}

struct GetFilterForShop: Codable {
    var status: Bool?
    var response = [FilterForShopResponse]()
}

struct FilterForShopResponse: Codable {
    var id: Int?
    var name_en: String?
    var name_ar: String?
    var name_ur: String?
    var name_es: String?
    var name_pt: String?
    var name_zh: String?
    var type: String?
    var is_required: Int?
    var attribute_slug: String?
    var is_filterable: Int?
    var attribute_option = [AttributeOption]()
}

struct AttributeOption: Codable {
    var id: Int?
    var value: String?
    var min: Int?
    var max: Int?
    
}
