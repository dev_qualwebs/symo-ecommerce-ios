//
//  SearchVendorViewController.swift
//  Symo New
//
//  Created by Apple on 01/04/21.
//

import UIKit

class SearchVendorViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var pinkView: UIView!
    @IBOutlet weak var vendorTable: UITableView!

    var vendorByCityData = [VendorResponse]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = K_PINK_COLOR
        self.pinkView.backgroundColor = K_PINK_COLOR
        
        NavigationController.shared.getvendorByCity { (response) in
            self.vendorByCityData = response
            self.vendorTable.reloadData()
        }
    }

    //MARK: IBActions
    @IBAction func menuAction(_ sender: Any) {
        self.menu()
    }
    
}

extension SearchVendorViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.vendorByCityData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryTableCell") as! CategoryTableCell
        let val = self.vendorByCityData[indexPath.row]
        cell.shopImage.sd_setImage(with: URL(string:U_PRODUCT_IMAGE_BASE + (val.avatar ?? "")), placeholderImage: #imageLiteral(resourceName: "nearbyshop"))
        cell.shopName.text = val.vendor_name
        cell.shopDistance.text = "\(Int(val.distance ?? 0)) kms"
        cell.totalReview.text = "\(val.reviews ?? 0) reviews"
        cell.ratingView.rating = Double(val.rating ?? 0)
        //cell.shopAddress.text =
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
}
