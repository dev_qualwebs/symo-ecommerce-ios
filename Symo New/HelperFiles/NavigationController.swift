//
//  NavigationController.swift
//  Hey Hala
//
//  Created by qw on 01/01/21.
//

import UIKit
import CoreLocation


class NavigationController: UIViewController, CLLocationManagerDelegate {
    //MARK: IBOutlets
    
    //Location MAP
    var onUserLocationReceived: ((_ userLocation: CLLocation) -> Void)? = nil
    
    static let shared = NavigationController()
    lazy var locationManager = CLLocationManager()
    let location = Singleton.shared.userLocation

    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
    }
    
    func registerEventListenerForLocation(withBlock block: @escaping (_ location: CLLocation) -> Void) {
        onUserLocationReceived = block
    }
    
    func getUserCurrentLocation() {
        initLocationManager()
    }
    
    func initLocationManager() {
        locationManager = CLLocationManager()
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func goToHomeVC(controller:UIViewController){
        let myVC = controller.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        controller.navigationController?.pushViewController(myVC, animated: true)
    }

    func getvendorByCity(completionHandler: @escaping ([VendorResponse]) -> Void){
        
        if(Singleton.shared.vendorsByCity.count == 0){
            let param:[String:Any] = [
                "limit": 50,
                "page": 1,
                "lang": Singleton.shared.selectedLanguage,
                "city_id": 4,
                "lat" : "22.6978622", //location.coordinate.latitude,
                "long" :  "75.8708085"//location.coordinate.longitude
            ]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_VENDOR_BY_CITY, method: .post, parameter: param, objectClass: GetVendor.self, requestCode: U_GET_VENDOR_BY_CITY, userToken: nil) { (response) in
                Singleton.shared.vendorsByCity = response.response.response
                completionHandler(response.response.response)
            }
        }else {
            completionHandler(Singleton.shared.vendorsByCity)
        }
    }
    
    func getNewAndTrending(completionHandler: @escaping (NewTrendingResponse) -> Void){
        if(Singleton.shared.newTrendingBusiness.new_business.count == 0 && Singleton.shared.newTrendingBusiness.trending_business.count == 0){
            let param:[String:Any] = [
                "lang": Singleton.shared.selectedLanguage,
                "city_id": 4,
            ]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_NEW_TRENDING_BUSINESS, method: .post, parameter: param, objectClass: GetNewTrending.self, requestCode: U_GET_NEW_TRENDING_BUSINESS, userToken: nil) { (response) in
                Singleton.shared.newTrendingBusiness = response.response
                completionHandler(response.response)
            }
        }else {
            completionHandler(Singleton.shared.newTrendingBusiness)
        }
    }
    
    func getShopByCategory(completionHandler: @escaping (NewTrendingResponse) -> Void){
        if(Singleton.shared.newTrendingBusiness.new_business.count == 0 && Singleton.shared.newTrendingBusiness.trending_business.count == 0){
            let param:[String:Any] = [
                "lang": Singleton.shared.selectedLanguage,
                "city_id": 4,
            ]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_NEW_TRENDING_BUSINESS, method: .post, parameter: param, objectClass: GetNewTrending.self, requestCode: U_GET_NEW_TRENDING_BUSINESS, userToken: nil) { (response) in
                Singleton.shared.newTrendingBusiness = response.response
                completionHandler(response.response)
            }
        }else {
            completionHandler(Singleton.shared.newTrendingBusiness)
        }
    }

    func getAllCategory(completionHandler: @escaping ([CategoryResponse]) -> Void){
        if(Singleton.shared.allCategoryData.count == 0){
            let url = U_BASE + U_GET_ALL_CATEGORY + pageLimitWithLang + Singleton.shared.selectedLanguage
            SessionManager.shared.methodForApiCalling(url: url, method: .get, parameter: nil, objectClass: GetCategory.self, requestCode: U_GET_ALL_CATEGORY, userToken: nil) { (response) in
                Singleton.shared.allCategoryData = response.response
                completionHandler(response.response)
            }
        }else {
            completionHandler(Singleton.shared.allCategoryData)
        }
    }

    func getAllSeasonalOffer(completionHandler: @escaping (SeasonalOffersResponse) -> Void){
        if(Singleton.shared.vendorsByCity.count == 0){
            let url = U_BASE + U_GET_SEASONAL_OFFER + "?city_id=\(4)&lat=22.6978622&long=75.8708085&lang=en"
            SessionManager.shared.methodForApiCalling(url: url, method: .get, parameter: nil, objectClass: GetSeasonalOffer.self, requestCode: U_GET_SEASONAL_OFFER, userToken: nil) { (response) in
                Singleton.shared.seasonalOffer = response.response
                    completionHandler(response.response)
            }
        }else {
            completionHandler(Singleton.shared.seasonalOffer)
        }
    }
     
    
    
    func hasLocPermission() -> Bool {
        var hasPermission = false
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                hasPermission = false
            case .authorizedAlways, .authorizedWhenInUse:
                hasPermission = true
            }
        } else {
            hasPermission = false
        }
        return hasPermission
    }
    
    func getCities(countryId: Int,completionHandler: @escaping (([Cities]) -> Void)) {
        if(Singleton.shared.cityData.count == 0){
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_CITY + "\(countryId)", method: .get, parameter: nil, objectClass: CityResponse.self, requestCode: U_GET_CITY, userToken: nil) { (cityResponse) in
                Singleton.shared.cityData = (cityResponse.cities)
                if(Singleton.shared.selectedCity.city_id == nil){
                    let city = try? JSONEncoder().encode(cityResponse.cities[0])
                    Singleton.shared.selectedCity = cityResponse.cities[0]
                    UserDefaults.standard.setValue(city, forKey: UD_SELECTED_CITY)
                }
                completionHandler((cityResponse.cities))
            }
        }else {
            completionHandler(Singleton.shared.cityData)
        }
    }
    
    func getCountries(completionHandler: @escaping (([Countries]) -> Void)) {
        if(Singleton.shared.countryData.count == 0){
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_COUNTRY, method: .get, parameter: nil, objectClass: CountryResponse.self, requestCode: U_GET_COUNTRY, userToken: nil) { (response) in
                Singleton.shared.countryData = (response.countries)
                if(Singleton.shared.selectedCountry.id == nil){
                    Singleton.shared.selectedCountry = response.countries[0]
                    let country = try? JSONEncoder().encode(response.countries[0])
                    UserDefaults.standard.setValue(country, forKey: UD_SELECTED_COUNTRY)
                }
                completionHandler(response.countries)
            }
        }else {
            completionHandler(Singleton.shared.countryData)
        }
    }
    
}




