//
//  SignupViewController.swift
//  Symo New
//
//  Created by Apple on 30/03/21.
//

import UIKit
import SkyFloatingLabelTextField

class SignupViewController: UIViewController, SelectFromPicker, SelectDate {
    func selectedPickerData(val: String, pos: Int) {
        if (self.currentPicker == 1){
        Singleton.shared.selectedCountry = Singleton.shared.countryData[pos]
        self.countryField.text = val
        Singleton.shared.cityData = []
        self.countryCode.text = Singleton.shared.selectedCountry.country_code
        NavigationController.shared.getCities(countryId: Singleton.shared.selectedCountry.id ?? 0) { (val) in
        }
        }else if(self.currentPicker == 2){
            Singleton.shared.selectedCity = Singleton.shared.cityData[pos]
                self.cityField.text = val
        }else if(self.currentPicker == 3){
            self.gender.text = val
        }
    }
    
    func selectedDate(timestamp: Date) {
        self.dobField.text = self.convertTimestampToDate(Int(timestamp.timeIntervalSince1970), to: "dd-MM-yy")
    }
    
    //MARK: IBOutlets
    
    @IBOutlet weak var signupLabel: DesignableUILabel!
    @IBOutlet weak var fullName: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var email: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var countryField: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var countryCode: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var contactNumber: UITextField!
    @IBOutlet weak var gender: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var dobField: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var password: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var confirmPassword: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var cityField: SkyFloatingLabelTextFieldWithIcon!
    
    @IBOutlet weak var pinkView: UIView!
    @IBOutlet weak var accountType: SkyFloatingLabelTextFieldWithIcon!

    
    var currentPicker = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
      //  self.view.backgroundColor = K_PINK_COLOR
        self.pinkView.backgroundColor = K_PINK_COLOR
        //TextFields
        let helper = Helper()
        let textFields = [fullName, email, countryField, gender, dobField, password, confirmPassword, cityField, accountType]
        for index in 0..<textFields.count{
            helper.setupFields(textField: textFields[index]! , index: index, form: K_SIGNUP_FORM)
        }
    }
    
    //MARK: IBActions
    
    @IBAction func signupAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func countryAction(_ sender: Any) {
        currentPicker = 1
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        myVC.pickerDelegate = self
        for val in Singleton.shared.countryData{
            myVC.pickerData.append(val.name_en ?? "")
        }
        myVC.modalPresentationStyle = .overCurrentContext
        if(myVC.pickerData.count > 0){
            self.navigationController?.present(myVC, animated: true, completion: nil)
            
        }
    }
    
    @IBAction func genderAction(_ sender: Any) {
        currentPicker = 3
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        myVC.pickerDelegate = self
        myVC.pickerData = ["Male", "Female"]
        myVC.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(myVC, animated: true, completion: nil)
    }
    
    @IBAction func cityAction(_ sender: Any) {
        currentPicker = 2
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        myVC.pickerDelegate = self
        for val in Singleton.shared.cityData{
            myVC.pickerData.append(val.name_en ?? "")
        }
        myVC.modalPresentationStyle = .overCurrentContext
        if(myVC.pickerData.count > 0){
            self.navigationController?.present(myVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func dobAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerViewController") as! DatePickerViewController
        myVC.dateDelegate = self
        myVC.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(myVC, animated: true, completion: nil)
    }
    
    @IBAction func registerAction(_ sender: Any) {
        if(self.fullName.text!.isEmpty){
            self.showErrorMsg(msg: "Enter Full Name")
        }else if(self.email.text!.isEmpty){
            self.showErrorMsg(msg: "Enter Email Address")
        }else if !(self.isValidEmail(emailStr: self.email.text ?? "")){
            self.showErrorMsg(msg: "Enter valid Email Address")
        }else if(self.countryField.text!.isEmpty){
            self.showErrorMsg(msg: "Select Country")
        }else if(self.contactNumber.text!.isEmpty){
            self.showErrorMsg(msg: "Enter Contact Number")
        }else if(self.gender.text!.isEmpty){
            self.showErrorMsg(msg: "Select Gender")
        }else if(self.dobField.text!.isEmpty){
            self.showErrorMsg(msg: "Enter your Date of Birth")
        }else if(self.password.text!.isEmpty){
            self.showErrorMsg(msg: "Enter Password")
        }else if(self.confirmPassword.text!.isEmpty){
            self.showErrorMsg(msg: "Enter confirm password")
        }else if(self.cityField.text!.isEmpty){
            self.showErrorMsg(msg: "Select City")
        }else if(self.accountType.text!.isEmpty){
            self.showErrorMsg(msg: "Select Account Type")
        }else {
            ActivityIndicator.show(view: self.view)
            let param:[String:Any] = [
                "name": self.fullName.text,
                "email":self.email.text,
                "password": self.password.text,
                "phone" :self.contactNumber.text,
                "city": self.cityField.text,
                "country": Singleton.shared.selectedCountry.id ?? 0,
                "country_code": self.countryCode.text
            ]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_SIGNUP, method: .post, parameter: param, objectClass: LoginResponse.self, requestCode: U_SIGNUP, userToken: nil) { (response) in
                ActivityIndicator.hide()
                self.showErrorMsg(msg: "Logged In Successfully")
                Singleton.shared.saveUserDetail(user: response.response)
                Singleton.shared.userDetail = response.response
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
                self.navigationController?.pushViewController(myVC, animated: true)
                
            }
        }
    }
}
