//
//  CategoryViewController.swift
//  Symo New
//
//  Created by Apple on 06/04/21.
//

import UIKit
import Cosmos
import SDWebImage

class CategoryViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var viewProductButtom: CustomButton!
    @IBOutlet weak var nearbyShopCollection: UICollectionView!
    
    
    @IBOutlet weak var shopImage: ImageView!
    @IBOutlet weak var vendorName: DesignableUILabel!
    @IBOutlet weak var vendorRating: CosmosView!
    @IBOutlet weak var totalReview: DesignableUILabel!
    @IBOutlet weak var mobileNumber: DesignableUILabel!
    @IBOutlet weak var address: DesignableUILabel!
    @IBOutlet weak var time: DesignableUILabel!
    @IBOutlet weak var vendorDescription: DesignableUILabel!
    
    var vendorId = Int()
    var vendorDetail = VendorDetail()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = K_PINK_COLOR
        self.viewProductButtom.backgroundColor = K_PINK_COLOR
        //self.getVendorDetail()
    }
    
    func getVendorDetail(){
        ActivityIndicator.show(view: self.view)
        let url = U_BASE + U_GET_VENDOR_DETAIL + "\(vendorId)?lang=" + Singleton.shared.selectedLanguage
        let param:[String:Any] = [
            "device_token":"dd"
        ]
        SessionManager.shared.methodForApiCalling(url: url, method: .post, parameter: param, objectClass:GetVendorDetail.self, requestCode: url, userToken: nil) { (response) in
            self.vendorDetail = response.response
            self.shopImage.sd_setImage(with: URL(string:U_IMAGE_BASE + (self.vendorDetail.avatar ?? "")), placeholderImage: #imageLiteral(resourceName: "nearbyshop"))
            self.vendorName.text = self.vendorDetail.vendor_name
            self.mobileNumber.text = self.vendorDetail.phone
            self.address.text = self.vendorDetail.address
            self.vendorDescription.text = self.vendorDetail.description
            ActivityIndicator.hide()
        }
        
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func viewProductAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ProductScreenViewController") as! ProductScreenViewController
        myVC.subdomain = self.vendorDetail.subdomain ?? ""
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func favouriteAction(_ sender: Any) {
    }
    
    
}

extension CategoryViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionCell", for: indexPath) as! CategoryCollectionCell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(collectionView == nearbyShopCollection){
            return CGSize(width: collectionView.frame.size.width/3-10, height: collectionView.frame.size.width/3-10)
        }else {
          return CGSize(width: collectionView.frame.size.width/3-20, height: collectionView.frame.size.width/3-20)
        }
    }
    
}
