//
//  SessionManager.swift
//  Hey hala
//
//  Created by qw on 14/01/21.
//

import UIKit
import Alamofire


class SessionManager: NSObject {

    static var shared = SessionManager()

    var createWallet: Bool = true

    func methodForApiCalling<T: Codable>(url: String, method: HTTPMethod, parameter: Parameters?, objectClass: T.Type, requestCode: String, userToken: String?, completionHandler: @escaping (T) -> Void) {
        print("URL: \(url)")
        print("METHOD: \(method)")
        print("PARAMETERS: \(parameter)")
        print("TOKEN: \(getHeader(reqCode: requestCode, userToken: userToken))")
        AF.request(url, method: method, parameters: parameter, encoding: JSONEncoding.default, headers:  getHeader(reqCode: requestCode, userToken: userToken), interceptor: nil).responseString { (dataResponse) in
            let statusCode = dataResponse.response?.statusCode
            print("statusCode: ",dataResponse.response?.statusCode)
            print("dataResponse: \(dataResponse)")

            switch dataResponse.result {
            case .success(_):
                var object:T?
                if(statusCode != 400){
                   object = self.convertDataToObject(response: dataResponse.data, T.self)
                }else if(statusCode == 200){
                   object = self.convertDataToObject(response: dataResponse.data, T.self)
                }
                let errorObject = self.convertDataToObject(response: dataResponse.data, ErrorResponse.self)
                if (statusCode == 200 || statusCode == 201) && object != nil{
                    completionHandler(object!)
                } else if statusCode == 404  {
                    NotificationCenter.default.post(name: NSNotification.Name(N_SHOW_ERROR_MESSAGE), object: nil, userInfo: ["msg": errorObject?.message ?? ""])
                }else if statusCode == 400{

                        if(errorObject?.message != "" || errorObject?.message != nil){

                        }
                    NotificationCenter.default.post(name: NSNotification.Name(N_SHOW_ERROR_MESSAGE), object: nil, userInfo: ["msg": errorObject?.message ?? ""])

                } else {
                    NotificationCenter.default.post(name: NSNotification.Name(N_SHOW_ERROR_MESSAGE), object: nil, userInfo: ["msg": errorObject?.message ?? ""])

                
                }
                ActivityIndicator.hide()
                break
            case .failure(_):
               // ActivityIndicator.hide()
                let error = dataResponse.error?.localizedDescription
                if error == "The Internet connection appears to be offline." {
                    //Showing error message on alert
                    if(error != "" || error != nil){
                    }
                   
                    NotificationCenter.default.post(name: NSNotification.Name(N_SHOW_ERROR_MESSAGE), object: nil, userInfo: ["msg": error ?? ""])
                } else {
                 
                    NotificationCenter.default.post(name: NSNotification.Name(N_SHOW_ERROR_MESSAGE), object: nil, userInfo: ["msg": error ?? ""])

                }
                break
            }
        }
    }

  


    private func convertDataToObject<T: Codable>(response inData: Data?, _ object: T.Type) -> T? {
        if let data = inData {
            do {
                let decoder = JSONDecoder()
                let decoded = try decoder.decode(T.self, from: data)
                return decoded
            } catch {
                print(error)
            }
        }
        return nil
    }


    func getHeader(reqCode: String, userToken: String?) -> HTTPHeaders? {
        var token = Singleton.shared.userDetail
        if (token.id != nil){
            if(token == nil){
                return nil
            }else {
                return ["Authorization": "Bearer " + "\(token.id ?? 0)"]
            }
            } else {
                return nil
            }
        }
    }
