//
//  Constants.swift
//  Hey Hala
//
//  Created by qw on 23/12/20.

import UIKit

var K_BLUE_COLOR = UIColor(red: 55/255, green: 193/255, blue: 240/255, alpha: 1)//#37C1F0
var K_PINK_COLOR = UIColor(red: 228/255, green: 28/255, blue: 100/255, alpha: 1)//#E41C64
var K_YELLOW_COLOR = UIColor(red: 242/255, green: 207/255, blue: 31/255, alpha: 1)//#f2cf1f
var K_LIGHT_PINK_COLOR = UIColor(red: 238/255, green: 92/255, blue: 134/255, alpha: 1) //#EE5C86
var K_BLACK_COLOR = UIColor(red: 61/255, green: 61/255, blue: 61/255, alpha: 1)//#3d3d3dff
var K_OFF_WHITE_COLOR = UIColor(red: 236/255, green: 236/255, blue: 236/255, alpha: 1)//#ececec

let U_BASE = "https://beta.gosymo.com/api/"
let U_PRODUCT_IMAGE_BASE =  "https://beta.gosymo.com/storage/app/public/product_image/"
let U_IMAGE_BASE = "https://beta.gosymo.com/storage/app/public/"

 
 let latitude = "&lat="
 let optionalLat = "?lat="
 let lang = "&lang="
 let optionalLang = "?lang="
 let page = "&page="
 let limit = "&limit=50"
 let pageLimitWithLang = "?page=1&limit=50&lang="
 let limitWithLang = "?limit=50&lang="

 let GOOGLE_API_KEY = "AIzaSyAuZcGvLi_RGZlK1gi_lkSq2dI9ax7Ng4U"



let U_LOGIN = "user/login"
let U_SIGNUP = "user/v2/signup"
let U_GET_COUNTRY = "user/get-country"
let U_GET_CITY = "user/get-cities-by-country/"

let U_GET_VENDOR_BY_CITY = "vendor-search"
let U_GET_NEW_TRENDING_BUSINESS = "get-new-and-trending-business"
let U_GET_VENDOR_DETAIL = "user/vendor/details/"

let U_GET_ALL_CATEGORY = "user/category"
let U_GET_SHOP_BY_CATEGORY = "user/search/category"

let U_GET_SEASONAL_OFFER = "user/home_page_data"

let U_GET_SHOP_PRODUCT = "shop-products/"

let U_GET_FILTER_SHOP = "get-filter-attributes-by-shop/"

let K_APP_LANGUAGE = "K_APP_LANGUAGE"
let K_LOGIN_FORM = "K_LOGIN_FORM"
let K_SIGNUP_FORM = "K_SIGNUP_FORM"
var K_SELECTED_SORTBY = 0

//User Defaults
let UD_USER_DETAIl = "userDetail"
let UD_TOKEN = "user_token"
let UD_SELECTED_COUNTRY = "UD_SELECTED_COUNTRY"
let UD_SELECTED_CITY = "UD_SELECTED_CITY"

//Notifications

let N_SHOW_ERROR_MESSAGE = "N_SHOW_ERROR_MESSAGE"
let N_HANDLE_LOCATION_PERMISSION = "N_HANDLE_LOCATION_PERMISSION"
