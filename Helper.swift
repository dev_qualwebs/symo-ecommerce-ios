//
//  Helper.swift
//  Symo New
//
//  Created by Apple on 31/03/21.
//


import Foundation
import SkyFloatingLabelTextField
import GoogleMaps
import GooglePlaces
import FirebaseInstanceID

class Helper {
    var loginCodes = ["\u{f003}" , "\u{f023}"]
    var loginFieldNames = [NSLocalizedString("Email Address", tableName: nil, bundle: bundle!, value: "", comment: ""),NSLocalizedString("Password", tableName: nil, bundle: bundle!, value: "", comment: "")]
    
    

    var signupCodes = ["\u{f007}" ,"\u{f003}", "\u{f041}","\u{f007}" ,"\u{1F4C5}", "\u{f023}", "\u{f023}", "\u{f041}","\u{f007}"]
    var signupFieldNames = [NSLocalizedString("Full Name", tableName: nil, bundle: bundle!, value: "", comment: ""),
                      NSLocalizedString("Email Address", tableName: nil, bundle: bundle!, value: "", comment: ""),
                      NSLocalizedString("Country", tableName: nil, bundle: bundle!, value: "", comment: ""),
                      NSLocalizedString("Gender", tableName: nil, bundle: bundle!, value: "", comment: ""),
    
                      NSLocalizedString("Date of Birth", tableName: nil, bundle: bundle!, value: "", comment: ""),
                      NSLocalizedString("Create Passsword", tableName: nil, bundle: bundle!, value: "", comment: ""),
                      NSLocalizedString("Confirm Password", tableName: nil, bundle: bundle!, value: "", comment: ""),
                      NSLocalizedString("City", tableName: nil, bundle: bundle!, value: "", comment: ""),
                      NSLocalizedString("Account Type", tableName: nil, bundle: bundle!, value: "", comment: ""),
    ]
    
    var codesForResetPassword = ["\u{f023}", "\u{f023}"]
    var fieldNamesForResetPassword = [NSLocalizedString("New Password", tableName: nil, bundle: bundle!, value: "", comment: ""),
                                      NSLocalizedString("Confirm Password", tableName: nil, bundle: bundle!, value: "", comment: "")]

    var codesForContact = ["\u{f007}" ,"\u{f003}" , "\u{f095}", "\u{f059}", "\u{f003}"]
    var fieldNamesForContact = [NSLocalizedString("Name", tableName: nil, bundle: bundle!, value: "", comment: ""),
                                NSLocalizedString("Email Address", tableName: nil, bundle: bundle!, value: "", comment: ""),
                                NSLocalizedString("Phone Number", tableName: nil, bundle: bundle!,  value: "", comment: "") ,
                                NSLocalizedString("Message Title / Reason", tableName: nil, bundle: bundle!, value: "", comment: ""),
                                NSLocalizedString("Message", tableName: nil, bundle: bundle!, value: "", comment: "")]
    
    var venforFormCodes = ["\u{f007}" , "\u{f003}" ,"\u{f023}" , "\u{f023}" ,"\u{f041}", "\u{f095}", "\u{f041}"]
    
    var vendorForm = [
        NSLocalizedString("Full Name", tableName: nil, bundle: bundle!, value: "", comment: ""),
        NSLocalizedString("Email Address", tableName: nil, bundle: bundle!, value: "", comment: "") ,
        NSLocalizedString("Password", tableName: nil, bundle: bundle!, value: "", comment: ""),
        NSLocalizedString("Confirm Password", tableName: nil, bundle: bundle!, value: "", comment: "") ,
          NSLocalizedString("Country", tableName: nil, bundle: bundle!, value: "", comment: "") ,
        NSLocalizedString("Phone Number", tableName: nil, bundle: bundle!, value: "", comment: "") ,
        NSLocalizedString("City", tableName: nil, bundle: bundle!, value: "", comment: "")]
    
    var form = ["\u{f007}", "\u{f007}", "\u{f54e}", "\u{f54e}","\u{f041}", "\u{f039}", "\u{f039}", "\u{f095}", "\u{f095}", "\u{f095}", "\u{f041}", "\u{f041}", "\u{f267}"]
    
    var businessForm = [NSLocalizedString("Name (en)", tableName: nil, bundle: bundle!, value: "", comment: ""),
                        NSLocalizedString("Name (ar)", tableName: nil, bundle: bundle!, value: "", comment: ""),
                        NSLocalizedString("Category", tableName: nil, bundle: bundle!, value: "", comment: "") ,
                        NSLocalizedString("Sub Category", tableName: nil, bundle: bundle!, value: "", comment: ""),
                         NSLocalizedString("Country", tableName: nil, bundle: bundle!, value: "", comment: ""),
                        NSLocalizedString("Description (en)", tableName: nil, bundle: bundle!, value: "", comment: ""),
                        NSLocalizedString("Description (ar)", tableName: nil, bundle: bundle!, value: "", comment: ""),
                        NSLocalizedString("Phone Number", tableName: nil, bundle: bundle!, value: "", comment: ""),
                        NSLocalizedString("Secondary Phone Number", tableName: nil, bundle: bundle!, value: "", comment: ""),
                        NSLocalizedString("Tertiary Phone Number", tableName: nil, bundle: bundle!, value: "", comment: ""),
                        NSLocalizedString("Address", tableName: nil, bundle: bundle!, value: "", comment: ""),
                        NSLocalizedString("Business City", tableName: nil, bundle: bundle!, value: "", comment: ""),
                        NSLocalizedString("Website", tableName: nil, bundle: bundle!, value: "", comment: "")]
    
    let iconColor: UIColor = UIColor(red: 43 / 255, green: 54 / 255, blue: 74 / 255, alpha: 1.0)
    
    //GMS MARKER
    var myLocationMarker: GMSMarker?
    
    func setupFields(textField: SkyFloatingLabelTextFieldWithIcon , index: Int, form: String) {
        
        applySkyscannerThemeWithIcon(textField: textField)
        
        setFieldIconAndText(textField: textField , index: index,form:form)
        
        applySkyscannerThemeWithIcon(textField: textField)
    }
    
    func applySkyscannerThemeWithIcon(textField: SkyFloatingLabelTextFieldWithIcon) {
        self.applySkyscannerTheme(textField: textField)
        textField.iconColor = UIColor.lightGray
        textField.selectedIconColor = iconColor//UIColor.darkGray
        textField.iconFont = UIFont(name: "FontAwesome", size: 15)
    }
    
    func applySkyscannerTheme(textField: SkyFloatingLabelTextField) {
        
        textField.titleFormatter = { $0 }
        textField.textColor = UIColor.darkGray
        textField.lineColor = UIColor.lightGray
        textField.selectedLineColor =  UIColor(red: 52 / 255, green: 192 / 255, blue: 240 / 255, alpha: 1.0)
        textField.selectedTitleColor = UIColor.black
        textField.lineHeight = 1.0
        textField.selectedLineHeight = 1.0
        
        // Set custom fonts for the title, placeholder and textfield labels
        textField.titleLabel.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 12)
        textField.placeholderFont = UIFont(name: "AppleSDGothicNeo-Light", size: 18)
        textField.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 18)
        
        if Singleton.shared.language == "ar" {
            textField.isLTRLanguage = false
        } else {
            textField.isLTRLanguage = true
        }
    }
    
    //Setting up Fields Icon and Text
    func setFieldIconAndText(textField: SkyFloatingLabelTextFieldWithIcon , index: Int, form: String){
        if(form == K_LOGIN_FORM){
            let code = loginCodes[index]
            let name = loginFieldNames[index]
            textField.iconText = code
            textField.placeholder = name
            textField.selectedTitle = name
            textField.title = name
        }else  if(form == K_SIGNUP_FORM){
            let code = signupCodes[index]
            let name = signupFieldNames[index]
            textField.iconText = code
            textField.placeholder = name
            textField.selectedTitle = name
            textField.title = name
        }
    }
    
    func image(_ originalImage: UIImage, scaledTo size: CGSize) -> UIImage {
        //avoid redundant drawing
        if originalImage.size.equalTo(size) {
            return originalImage
        }
        //create drawing context
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        //draw
        originalImage.draw(in: CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height))
        //capture resultant image
        let image = UIGraphicsGetImageFromCurrentImageContext() as? UIImage
        UIGraphicsEndImageContext()
        return image ?? UIImage()
    }
    
    func initMap(withLatitude lat: CLLocationDegrees, longitude lng: CLLocationDegrees, zoom: Int , googleMapView: GMSMapView) {
        defer {
        }
        do {
            let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lng, zoom: Float(zoom))
            googleMapView.isMyLocationEnabled = true
            googleMapView.camera = camera
            // Creates a marker in the center of the map.
            myLocationMarker?.map = googleMapView
        } catch let ex {
           
        }
    }
    
    func getDay(day : Int) -> String {
        switch day {
        case 1:
            return "Monday"
        case 2:
            return "Tuesday"
        case 3:
            return "Wednesday"
        case 4:
            return "Thursday"
        case 5:
            return "Friday"
        case 6:
            return "Saturday"
        case 7:
            return "Sunday"
        default:
            return ""
        }
    }

    class func dropShadow(color: UIColor, opacity: Float = 5, offSet: CGSize, radius: CGFloat = 5, object: UIView) {
        object.layer.masksToBounds = false
        object.layer.shadowColor = color.cgColor
        object.layer.shadowOpacity = opacity
        object.layer.shadowOffset = offSet
        object.layer.shadowRadius = radius
    }
}

